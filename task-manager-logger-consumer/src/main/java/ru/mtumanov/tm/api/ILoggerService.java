package ru.mtumanov.tm.api;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ILoggerService {

    void log(@NotNull String message) throws IOException;

}

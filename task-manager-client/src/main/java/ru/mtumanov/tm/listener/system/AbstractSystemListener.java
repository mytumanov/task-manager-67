package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}

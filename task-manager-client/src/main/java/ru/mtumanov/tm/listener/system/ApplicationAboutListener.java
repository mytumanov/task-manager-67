package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class ApplicationAboutListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String getArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show about program";
    }

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("e-mail: " + getPropertyService().getAuthorEmail());
    }

}

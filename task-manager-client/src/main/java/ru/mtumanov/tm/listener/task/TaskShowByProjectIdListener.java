package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskShowByProjectIdRq;
import ru.mtumanov.tm.dto.response.task.TaskShowByProjectIdRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskShowByProjectIdListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Show tasks by project id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRq request = new TaskShowByProjectIdRq(getToken(), projectId);
        @NotNull final TaskShowByProjectIdRs response = getTaskEndpoint().taskShowByProjectId(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        renderTask(response.getTasks());
    }

}

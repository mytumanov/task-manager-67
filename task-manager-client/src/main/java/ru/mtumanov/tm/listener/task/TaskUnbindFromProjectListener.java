package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskUnbindFromProjectRq;
import ru.mtumanov.tm.dto.response.task.TaskUnbindFromProjectRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Unbind task from project";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTERE TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRq request = new TaskUnbindFromProjectRq(getToken(), taskId, projectId);
        @NotNull final TaskUnbindFromProjectRs response = getTaskEndpoint().taskUnbindFromProject(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

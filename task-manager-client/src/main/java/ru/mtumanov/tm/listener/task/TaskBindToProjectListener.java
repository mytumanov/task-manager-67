package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskBindToProjectRq;
import ru.mtumanov.tm.dto.response.task.TaskBindToProjectRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskBindToProjectListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Bind task to project";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRq request = new TaskBindToProjectRq(getToken(), taskId, projectId);
        @NotNull final TaskBindToProjectRs response = getTaskEndpoint().taskBindToProject(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

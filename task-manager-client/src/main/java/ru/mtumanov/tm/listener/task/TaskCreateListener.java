package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskCreateRq;
import ru.mtumanov.tm.dto.response.task.TaskCreateRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-create";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRq request = new TaskCreateRq(getToken(), name, description);
        @NotNull final TaskCreateRs response = getTaskEndpoint().taskCreate(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

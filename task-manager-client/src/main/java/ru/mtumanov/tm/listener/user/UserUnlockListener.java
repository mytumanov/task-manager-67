package ru.mtumanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserUnlockRq;
import ru.mtumanov.tm.dto.response.user.UserUnlockRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserUnlockListener extends AbstractUserListener {

    @Override
    @NotNull
    public String getDescription() {
        return "unlock user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-unlock";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRq request = new UserUnlockRq(getToken(), login);
        @NotNull final UserUnlockRs response = getUserEndpoint().userUnlock(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractListener> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractListener> mapByName = new TreeMap<>();

    @Override
    @NotNull
    public Collection<AbstractListener> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull AbstractListener abstractCommand) {
        @Nullable final String name = abstractCommand.getName();
        if (name != null && !name.isEmpty())
            mapByName.put(name, abstractCommand);
        @Nullable final String argument = abstractCommand.getArgument();
        if (argument != null && !argument.isEmpty())
            mapByArgument.put(argument, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractListener getCommandByName(@NotNull String name) {
        if (name.isEmpty())
            return null;
        return mapByName.get(name);
    }

    @Override
    @Nullable
    public AbstractListener getCommandByArgument(@NotNull String arg) {
        if (arg.isEmpty())
            return null;
        return mapByArgument.get(arg);
    }

    @Override
    @NotNull
    public Collection<AbstractListener> getCommandsWithArgument() {
        return mapByArgument.values();
    }

}

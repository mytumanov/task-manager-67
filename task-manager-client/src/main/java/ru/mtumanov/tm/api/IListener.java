package ru.mtumanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void handler(ConsoleEvent consoleEvent) throws AbstractException;
}

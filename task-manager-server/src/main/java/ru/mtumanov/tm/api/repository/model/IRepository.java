package ru.mtumanov.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.mtumanov.tm.model.AbstractModel;

@NoRepositoryBean
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}

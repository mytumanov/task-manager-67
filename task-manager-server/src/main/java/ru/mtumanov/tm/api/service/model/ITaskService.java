package ru.mtumanov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException;

}

package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import ru.mtumanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId, Sort sort);

}

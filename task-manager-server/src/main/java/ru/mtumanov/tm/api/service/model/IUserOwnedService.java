package ru.mtumanov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.EntitySort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable EntitySort sort);

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    void remove(@NotNull String userId, @NotNull M model);

    @NotNull
    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    long getSize(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

}

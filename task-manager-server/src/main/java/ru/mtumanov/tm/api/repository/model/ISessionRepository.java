package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import ru.mtumanov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    List<Session> findAllByUserId(@NotNull String userId, Sort sort);

}

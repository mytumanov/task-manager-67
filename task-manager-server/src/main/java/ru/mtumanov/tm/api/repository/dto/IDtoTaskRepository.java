package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface IDtoTaskRepository extends IDtoUserOwnedRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTaskByProjectId(@NotNull String projectId);

}

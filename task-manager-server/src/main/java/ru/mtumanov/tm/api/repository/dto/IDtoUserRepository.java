package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.UserDTO;

@Repository
public interface IDtoUserRepository extends IDtoRepository<UserDTO> {

    @NotNull
    UserDTO findByLogin(@NotNull String login);

    @NotNull
    UserDTO findByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}

package ru.mtumanov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    void set(@NotNull Collection<M> models) throws AbstractException;

}

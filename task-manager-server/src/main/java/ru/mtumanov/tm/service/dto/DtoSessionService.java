package ru.mtumanov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoSessionRepository;
import ru.mtumanov.tm.api.service.dto.IDtoSessionService;
import ru.mtumanov.tm.dto.model.SessionDTO;

@Service
public class DtoSessionService extends DtoAbstractUserOwnedService<SessionDTO, IDtoSessionRepository> implements IDtoSessionService {

}

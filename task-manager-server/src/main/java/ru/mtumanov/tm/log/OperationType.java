package ru.mtumanov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}

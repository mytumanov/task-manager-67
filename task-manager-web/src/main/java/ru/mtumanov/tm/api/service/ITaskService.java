package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService {

    void clear();

    @NotNull
    TaskDTO add(@Nullable final TaskDTO project);

    @NotNull
    TaskDTO update(@Nullable final TaskDTO project);

    @Nullable
    List<TaskDTO> findAll();

    void remove(@Nullable final TaskDTO project);

    void removeById(@Nullable final String id);

    @Nullable
    TaskDTO findById(@Nullable final String id);

}

package ru.mtumanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.TaskDTO;

@Repository
public interface ITaskRepository extends JpaRepository<TaskDTO, String> {

}

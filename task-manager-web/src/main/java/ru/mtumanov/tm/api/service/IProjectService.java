package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService {

    void clear();

    @NotNull
    ProjectDTO add(@Nullable final ProjectDTO project);

    @NotNull
    ProjectDTO update(@Nullable final ProjectDTO project);

    @Nullable
    List<ProjectDTO> findAll();

    void remove(@Nullable final ProjectDTO project);

    void removeById(@Nullable final String id);

    @Nullable
    ProjectDTO findById(@Nullable final String id);

}

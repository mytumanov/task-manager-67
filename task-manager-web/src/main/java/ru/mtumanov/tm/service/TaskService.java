package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO add(@Nullable final TaskDTO task) {
        if (task == null)
            throw new IllegalArgumentException();
        task.setUserId("d885e880-2f7f-4f85-a44f-b365dfc3ba42");
        return taskRepository.save(task);
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO update(@Nullable final TaskDTO task) {
        if (task == null)
            throw new IllegalArgumentException();
        task.setUserId("d885e880-2f7f-4f85-a44f-b365dfc3ba42");
        return taskRepository.save(task);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable final TaskDTO task) {
        if (task == null)
            throw new IllegalArgumentException();
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        taskRepository.deleteById(id);
    }

    @Override
    @Nullable
    @Transactional
    public TaskDTO findById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        return taskRepository.findById(id).orElse(null);
    }

}

package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.IProjectRestEndpoint;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @NotNull
    @WebMethod
    @GetMapping
    public Collection<ProjectDTO> findAll() {
        List<ProjectDTO> projects = projectService.findAll();
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/{id}")
    public ProjectDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id) {
        return projectService.findById(id);
    }

    @Override
    @NotNull
    @WebMethod
    @PostMapping
    public ProjectDTO create(
            @WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project) {
        return projectService.add(project);
    }

    @Override
    @WebMethod
    @DeleteMapping
    public void delete(
            @WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    public void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id) {
        projectService.removeById(id);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping
    public ProjectDTO update(
            @WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project) {
        return projectService.update(project);
    }

}

package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.ITaskRestEndpoint;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @WebMethod
    @GetMapping
    public Collection<TaskDTO> findAll() {
        List<TaskDTO> projects = taskService.findAll();
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/{id}")
    public TaskDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id) {
        return taskService.findById(id);
    }

    @Override
    @NotNull
    @WebMethod
    @PostMapping
    public TaskDTO create(@WebParam(name = "task", partName = "task") @RequestBody @Nullable final TaskDTO task) {
        return taskService.add(task);
    }

    @Override
    @WebMethod
    @DeleteMapping
    public void delete(@WebParam(name = "task", partName = "task") @RequestBody @Nullable final TaskDTO task) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    public void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id) {
        taskService.removeById(id);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping
    public TaskDTO update(@WebParam(name = "task", partName = "task") @RequestBody @Nullable final TaskDTO task) {
        return taskService.update(task);
    }
}

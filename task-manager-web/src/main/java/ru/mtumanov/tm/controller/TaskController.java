package ru.mtumanov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Status;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.add(new TaskDTO("Task name", "Task description"));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") @NotNull final String id) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") @NotNull final TaskDTO task) {
        taskService.update(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) {
        @Nullable final TaskDTO project = taskService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", project);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

    private Collection<ProjectDTO> getProjects() {
        return projectService.findAll();
    }

}

package ru.mtumanov.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.mtumanov.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.mtumanov.tm.api.repository")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @Value("${database.driver}") String databaseDriver,
            @Value("${database.url}") String databaseUrl,
            @Value("${database.username}") String databaseUser,
            @Value("${database.password}") String databasePassword) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("${database.dialect}") String databaseDialect,
            @Value("${database.hbm2ddl_auto}") String databaseHbm2ddl,
            @Value("${database.show_sql}") String databaseShowSql,
            @Value("${database.format_sql}") String databaseFormatSql

    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.mtumanov.tm.model", "ru.mtumanov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddl);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.FORMAT_SQL, databaseFormatSql);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}

package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.client.IProjectEndpointClient;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collection;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/project";

    @NotNull
    private IProjectEndpointClient projectEndpointClient = IProjectEndpointClient.projectClient(BASE_URL);

    @NotNull
    private Collection<ProjectDTO> projects = new ArrayList<>();

    @Before
    public void init() {
        @NotNull ProjectDTO project1 = new ProjectDTO("Project 1", "Project 1 desc", Status.NOT_STARTED);
        @NotNull ProjectDTO project2 = new ProjectDTO("Project 2", "Project 2 desc", Status.IN_PROGRESS);
        @NotNull ProjectDTO project3 = new ProjectDTO("Project 3", "Project 3 desc", Status.COMPLETED);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectEndpointClient.create(project1);
        projectEndpointClient.create(project2);
        projectEndpointClient.create(project3);
    }

    @After
    public void clear() {
        for (ProjectDTO project : projectEndpointClient.findAll()) {
            projectEndpointClient.delete(project);
        }
    }

    @Test
    public void findById() {
        for (ProjectDTO project : projects) {
            Assert.assertEquals(project, projectEndpointClient.findById(project.getId()));
        }
    }

    @Test
    public void findAll() {
        Assert.assertEquals(projects, projectEndpointClient.findAll());
    }

    @Test
    public void deleteById() {
        for (ProjectDTO project : projects) {
            projectEndpointClient.delete(project.getId());
            Assert.assertNull(projectEndpointClient.findById(project.getId()));
        }
    }

    @Test
    public void deleteByEntity() {
        for (ProjectDTO project : projects) {
            projectEndpointClient.delete(project);
            Assert.assertNull(projectEndpointClient.findById(project.getId()));
        }
    }

    @Test
    public void create() {
        @NotNull ProjectDTO projectToCreate = new ProjectDTO("Project test", "Project test desc", Status.NOT_STARTED);
        projectEndpointClient.create(projectToCreate);
        Assert.assertEquals(projects, projectEndpointClient.findAll());
    }

}

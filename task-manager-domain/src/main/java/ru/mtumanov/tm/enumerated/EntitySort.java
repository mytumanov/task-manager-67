package ru.mtumanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

public enum EntitySort {

    BY_NAME("Sort by name", Sort.by("name")),
    BY_STATUS("Sort by status", Sort.by("status")),
    BY_CREATED("Sort by created", Sort.by("created"));

    @NotNull
    private final String displayName;

    @NotNull
    private final Sort sort;

    EntitySort(@NotNull final String displayName, @NotNull final Sort sort) {
        this.displayName = displayName;
        this.sort = sort;
    }

    @Nullable
    public static EntitySort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return null;
        for (final EntitySort sort : values()) {
            if (sort.name().equals(value))
                return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Sort getSort() {
        return sort;
    }

}
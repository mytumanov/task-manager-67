package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public final class TaskBindToProjectRs extends AbstractResultRs {

    public TaskBindToProjectRs(@NotNull final Throwable err) {
        super(err);
    }

}
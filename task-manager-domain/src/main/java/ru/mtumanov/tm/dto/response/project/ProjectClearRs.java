package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public final class ProjectClearRs extends AbstractResultRs {

    public ProjectClearRs(@NotNull final Throwable err) {
        super(err);
    }
}
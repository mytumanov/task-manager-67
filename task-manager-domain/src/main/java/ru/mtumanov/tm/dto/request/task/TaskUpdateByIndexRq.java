package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIndexRq(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.index = index;
        this.name = name;
        this.description = description;
    }

}
package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false)
    protected String id = UUID.randomUUID().toString();

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AbstractModelDTO)) {
            return false;
        }
        AbstractModelDTO abstractModel = (AbstractModelDTO) o;
        return Objects.equals(id, abstractModel.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}
